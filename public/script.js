/*global jQuery*/
jQuery(document).ready(function ($) {
	$("#lightSlider").lightSlider({
			item: 4,
			infinite: true,
			slideMove: 4,
			speed: 1000,
			responsive: [
					{
							breakpoint: 500,
							settings: {
									item: 2,
									slideMove: 2
							}
					}
			]
	});
});
